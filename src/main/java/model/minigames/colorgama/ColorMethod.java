package model.minigames.colorgama;

/**
 * Enumeration of possible methods to represent colors.
 *
 */
public enum ColorMethod {

    /**
     * RGB color value.
     */
    RGB_VALUE,

    /**
     * HSB color value.
     */
    HSB_VALUE;
}
