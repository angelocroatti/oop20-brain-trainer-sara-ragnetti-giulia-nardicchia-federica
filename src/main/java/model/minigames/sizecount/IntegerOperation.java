package model.minigames.sizecount;

/**
 * A interface represent an operation between integers.
 *
 */
public interface IntegerOperation {

    /**
     * Compute the result of the operation.
     * 
     * @return the result of the operation
     */
    Integer getResult();
}
